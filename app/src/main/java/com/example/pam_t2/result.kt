package com.example.pam_t2

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView

class result : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        var getText: TextView = findViewById(R.id.getNama)
        var getJenisKelamin: TextView = findViewById(R.id.getJenisKelamin)
        var getProdi: TextView = findViewById(R.id.getProdi)
        val backbtn: ImageButton = findViewById(R.id.imageButton)
        backbtn.setOnClickListener {
            finish()
        }

        val bun: Bundle? = intent.extras

        if (bun !== null){

            getText.text = bun.getString("text")
            getProdi.text = bun.getString("prodi")
            getJenisKelamin.text = bun.getString("radio")
        }else {
            getText.text = "Tidak ada"
            getJenisKelamin.text = "null"
            getProdi.text = "null"
        }
    }




}