package com.example.pam_t2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import com.example.pam_t2.R.id.isiText

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    val prodi = arrayOf("Teknik Informatika", "Teknik Komputer", "Sistem Informasi", "Teknologi Informasi", "Pendidikan Teknologi Informasi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val spin: Spinner = findViewById(R.id.dropProdi)
        val btn1: Button = findViewById(R.id.btn)
        spin.onItemSelectedListener = this
        val ad: ArrayAdapter<String> = ArrayAdapter(this,android.R.layout.simple_spinner_item,prodi)

        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spin.adapter = ad

        btn1.setOnClickListener(){
            val intent= Intent(this, result::class.java)
            var isiText : EditText = findViewById(R.id.isiText)
            var JenisKelamin = findViewById<RadioGroup?>(R.id.radioGroup)
            val prodi = spin.selectedItem.toString()
            if (isiText.text.toString().isEmpty() || JenisKelamin.checkedRadioButtonId == -1  || prodi.isEmpty()){
                Toast.makeText(this, "Tidak boleh kosong", Toast.LENGTH_SHORT).show()
            } else {
                var rd: RadioButton = findViewById(JenisKelamin.checkedRadioButtonId)
                val bun = Bundle()
                bun.putString("text", isiText.text.toString())
                bun.putString("radio",rd.text.toString())
                bun.putString("prodi", prodi)
                intent.putExtras(bun)
                startActivity(intent)
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        findViewById<TextView>(isiText).text = ""
        findViewById<RadioGroup>(R.id.radioGroup).clearCheck()
        findViewById<Spinner>(R.id.dropProdi).setSelection(0)
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {

    }

    override fun onNothingSelected(parent: AdapterView<*>) {
    }


}